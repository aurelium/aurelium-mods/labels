# Copyright (C) 2019 za267@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################
#!/bin/bash

# TODO: Remove all of the hardcoded paths.

rsync -avz ./mods /home/someguy/.minetest/

# Generate world.mt file
cat /home/someguy/secret > /home/someguy/.minetest/worlds/Aurelium5/world.mt
echo '# Module List #' >> /home/someguy/.minetest/worlds/Aurelium5/world.mt
cat world.mt.mods >> /home/someguy/.minetest/worlds/Aurelium5/world.mt

## CUSTOM MODULE MANIPULATION ######################################
## This section is for handling non-standard install directories.
####################################################################
# 1) Terumet is a pain.
## Clean up previous deploy dir if exists
if [ -d /home/someguy/.minetest/mods/terumet.deploy ]; then
  echo "Dir was not deleted on previous deploy..."
  rm -Rf /home/someguy/.minetest/mods/terumet.deploy
fi
mv /home/someguy/.minetest/mods/terumet /home/someguy/.minetest/mods/terumet.deploy
mv /home/someguy/.minetest/mods/terumet.deploy/terumet /home/someguy/.minetest/mods/
rm -Rf /home/someguy/.minetest/mods/terumet.deploy
# 2) Ethereal needs some customization
## TODO add ethereal customizations.

## Add soft dependency of prefab_redo to signs_lib
if [ -f /home/someguy/.minetest/mods/signs_lib/depends.txt ]; then
  echo "prefab_redo?" >> /home/someguy/.minetest/mods/signs_lib/depends.txt
fi

# Final step: Set permissions
chown -R someguy: /home/someguy/.minetest/
sudo restorecon -RFv /home/someguy/.minetest/